﻿using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace WebApplication1.Areas.Manage.Controllers
{
    [Area("Manage")]

    public class AccountController : Controller
    {
        private readonly AppDbContext _dbContext;
        private readonly UserManager<ManagementUser> _userManager;
        private readonly SignInManager<ManagementUser> _signInManager;
        private readonly Tenant _tenant;
        private readonly ILogger<AccountController> _logger;

        public AccountController(
            AppDbContext dbContext,
            UserManager<ManagementUser> userManager,
            SignInManager<ManagementUser> signInManager,
            Tenant tenant,
            ILogger<AccountController> logger)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _tenant = tenant;
            _logger = logger;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            ViewData["ReturnUrl"] = Url.Action("Index", "Home", new {  area = "Manage" });
            return View(new LoginVM());
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginVM model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    return _redirectToLocal(returnUrl);
                }

                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ManagementUser
            {
                Id = Guid.NewGuid().ToString("D"),
                Email = model.Email,
                UserName = model.Email,
                FirstName = model.FirstName,
                Insertion = model.Insertion,
                LastName = model.LastName
            };

            var identityResult = await _userManager.CreateAsync(user, model.Password);

            if (!identityResult.Succeeded)
            {
                foreach (var error in identityResult.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }

                return BadRequest(model);
            }

            await _dbContext.SaveChangesAsync();

            return Ok();
        }

        private IActionResult _redirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}
