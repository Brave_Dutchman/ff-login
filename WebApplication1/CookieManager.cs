﻿using Database;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace WebApplication1
{
    public class CookieManager : ICookieManager
    {
        private readonly ICookieManager _concreteManager;

        public CookieManager()
        {
            _concreteManager = new ChunkingCookieManager();
        }

        public void AppendResponseCookie(HttpContext context, string key, string value, CookieOptions options)
        {
            var dbContext = _getDbContext(context);
            var tenant = dbContext.Tenants.SingleOrDefault(a => a.Domain == context.Request.Host.Host);

            options.Domain = tenant.CookieDomain;
            _concreteManager.AppendResponseCookie(context, tenant.CookieName, value, options);
        }

        public void DeleteCookie(HttpContext context, string key, CookieOptions options)
        {
            var dbContext = _getDbContext(context);
            var tenant = dbContext.Tenants.SingleOrDefault(a => a.Domain == context.Request.Host.Host);

            options.Domain = tenant.CookieDomain;
            _concreteManager.DeleteCookie(context, tenant.CookieName, options);
        }

        public string GetRequestCookie(HttpContext context, string key)
        {
            var dbContext = _getDbContext(context);
            var tenant = dbContext.Tenants.SingleOrDefault(a => a.Domain == context.Request.Host.Host);

            return _concreteManager.GetRequestCookie(context, tenant.CookieName);
        }

        private AppDbContext _getDbContext(HttpContext context)
        {
            return context.RequestServices.GetRequiredService<AppDbContext>();
        }
    }
}
