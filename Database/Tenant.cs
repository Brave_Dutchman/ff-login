﻿using System;

namespace Database
{
    public class Tenant 
    {
        public Guid Id { get; set; }

        /// <summary>
        /// This is attribute is set when a new tenant is made by the application, used for folder names.. etc
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The domain that the tenant will be identified with
        /// </summary>
        public string Domain { get; set; }

        public string CookieDomain { get; set; }

        public string CookieName { get; set; }
    }
}
