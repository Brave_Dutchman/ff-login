﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SaasKit.Multitenancy;
using System.Linq;
using System.Threading.Tasks;

namespace Database
{
    /// <summary>
    /// This class will be used to identify the associated tenant for a given request.
    /// If a tenant is found it returns a TenantContext<Tenant>, if no tenant can be resolved it returns null.
    /// Inspired by: https://andrewlock.net/loading-tenants-from-the-database-with-saaskit-in-asp-net-core/
    /// </summary>
    public class TenantDomainResolver : ITenantResolver<Tenant>
    {
        private readonly AppDbContext _dbContext;
        private readonly ILogger<TenantDomainResolver> _logger;

        public TenantDomainResolver(AppDbContext context, ILogger<TenantDomainResolver> logger)
        {
            _dbContext = context;
            _logger = logger;
        }

        public async Task<TenantContext<Tenant>> ResolveAsync(HttpContext context)
        {
            TenantContext<Tenant> tenantContext = null;
            var hostName = context.Request.Host.Host.ToLower();

            var tenant = await _dbContext.Tenants
                .FirstOrDefaultAsync(t => t.Domain.Equals(hostName));

            if (tenant != null)
            {
                tenantContext = new TenantContext<Tenant>(tenant);
            }
            else
            {
                _logger.LogWarning($"Unknown host '{hostName}' Target '{context.Request.Host}' " +
                    $"from '{context.Request.Headers["X-Forwarded-For"].FirstOrDefault()}'");
            }

            return await Task.FromResult(tenantContext);
        }
    }
}
