﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SaasKit.Multitenancy;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Database
{
    public class TenantUserStore : UserStore<ManagementUser, ManagementRole, AppDbContext>, IUserSecurityStampStore<ManagementUser>
    {
        private readonly TenantContext<Tenant> _tenantContext;

        public TenantUserStore(
            TenantContext<Tenant> tenantContext,
            AppDbContext context,
            IdentityErrorDescriber describer = null) 
            : base(context, describer)
        {
            _tenantContext = tenantContext;
        }

        protected override async Task<ManagementUser> FindUserAsync(string userId, CancellationToken cancellationToken)
        {
            var user = await base.FindUserAsync(userId, cancellationToken);

            return user.Tenant.Id == _tenantContext.Tenant.Id ? user : null;
        }

        public override Task<IdentityResult> CreateAsync(ManagementUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            user.Tenant = _tenantContext.Tenant;

            return base.CreateAsync(user, cancellationToken);
        }

        public override async Task<ManagementUser> FindByIdAsync(string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var user = await base.FindByIdAsync(userId, cancellationToken);

            return user?.Tenant.Id == _tenantContext.Tenant.Id ? user : null;
        }

        public override async Task<ManagementUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken = default(CancellationToken))
        {
            var user = await base.FindByEmailAsync(normalizedEmail, cancellationToken);

            return user.Tenant.Id == _tenantContext.Tenant.Id ? user : null;
        }

        public override IQueryable<ManagementUser> Users
        {
            get
            {
                return base.Users.Include(u => u.Tenant)
                    .Where(u => u.Tenant.Id == _tenantContext.Tenant.Id);
            }
        }

        public override Task<IdentityResult> DeleteAsync(ManagementUser user, CancellationToken cancellationToken = default(CancellationToken))
        {
            return user.Tenant.Id != _tenantContext.Tenant.Id
                ? Task.FromResult(IdentityResult.Failed(new IdentityError { Code = "Tenant error", Description = "User not found" }))
                : base.DeleteAsync(user, cancellationToken);
        }
    }
}
