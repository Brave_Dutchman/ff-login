﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Database
{
    public class ManagementUser : IdentityUser
    {
        public Guid? TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }

        public string FirstName { get; set; }

        public string Insertion { get; set; }

        public string LastName { get; set; }
    }
}
